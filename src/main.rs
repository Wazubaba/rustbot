#![allow(non_snake_case)]
use std::io::prelude::*;
use std::net::TcpStream;

mod utils;
mod parser;
mod iotest;
mod msgq;

static NAME: &'static str = "RustBot";
//static SERV: &'static str = "irc.scrumbleship.com";
//static CHAN: &'static str = "#scrumbleship-offtopic";
static SERV: &'static str = "server";
static CHAN: &'static str = "#wazuhome";

const  PORT: u16          = 6667;

fn main()
{
    iotest::test();
    iotest::test2();
    let socket = TcpStream::connect((SERV, PORT)).unwrap();
    let mut joined = false;
    
    utils::put(socket.try_clone().unwrap(), &format!("NICK {}\r\nUSER {} {} {} :{}\r\n", NAME, NAME, NAME, NAME, NAME));

    loop {
        // Keep alive
        // Hi Dirkson!
        let msg:&str = &msgq::getl(socket.try_clone().unwrap());
        if joined == false{
            if msg.contains("396") {
                joined = true;
                println!("\n\tJoining channel!\n");
                utils::put(socket.try_clone().unwrap(), &format!("JOIN {}\r\n", CHAN));
            } else
            if msg.contains("266") {
                joined = true;
                println!("\n\tJoining channel!\n");
                utils::put(socket.try_clone().unwrap(), &format!("JOIN {}\r\n", CHAN));
            }
        }

        let data = parser::stream_parse(msg);
        if data.hst != "NULLHST" {
            println!("<{}> {}", data.usr, data.msg);

            if data.msg == "!test" {
                println!("Sending to channel->'{}'", data.chn);
                utils::put(socket.try_clone().unwrap(), &format!("PRIVMSG {} :ohaider\r\n", data.chn));
            } else
            if data.msg == "!sd" {
                println!("Shutdown event recieved!");
                utils::put(socket.try_clone().unwrap(), &format!("PRIVMSG {} :Shutting down \\o\r\n", data.chn));
                break;
            }
        } else {
            print!("{}", data.msg);
        }
        if utils::subs(0, 4, msg) == "ERROR" {
            println!("Something just went very very wrong, shutting down...");
            break;
        }
        if utils::subs(0, 3, msg) == "PING" {
            println!("Sending PONG...");

            let mut holder = msg.split(" ");
            let _ = holder.next();
            let resp = holder.next().unwrap().lines_any().next().unwrap();
            utils::put(socket.try_clone().unwrap(), &format!("PONG {}\r\n", resp));
        }
    }
}

