use std::io::prelude::*;
use std::path::Path;

extern crate wio;

static CFG: &'static str = "testfile.log";

pub fn test()
{
    let mut file = match wio::fopen(CFG, "a") {
        Ok(File) => File,
        Err(err) => panic!("Error!: WTFBBQ!? {}", err),
    };

    let _ = file.write("\nDOGS \\o/\n\n".as_bytes());
}

pub fn test2()
{
    let path = Path::new(CFG);
    let mut file = match wio::fopenp(path, "a") {
        Ok(File) => File,
        Err(err) => panic!("AURW)(GHTJ)(:TG: {}", err),
    };

    let _ = file.write("\n\ttacos \\o/\n\n".as_bytes());
}

