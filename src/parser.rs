pub struct ChanData<'a>
{
    pub msg: &'a str, // Message
    pub hst: &'a str, // HostName
    pub chn: &'a str, // Channel
    pub usr: &'a str, // UserName
}

pub fn stream_parse(data: &str) -> ChanData
{ // Generates a ChanData struct containing parsed data
    if data.contains("PRIVMSG") {
        let mut msg: &str;
        let mut hst: &str;
        let mut chn: &str;
        let mut usr: &str;
        
        let mut work  = data.split("PRIVMSG"); // ":USR!HOST "|" CHAN :MSG\r\n"
        { // This might help reduce overhead? idk for sure though...
            let w1 = work.next().unwrap(); //":USR!HST "
            let mut w1a = w1.split("!");   //":USR"|"HST "
            usr = w1a.next().unwrap().trim_matches(':');
            hst = w1a.next().unwrap().trim_matches(' ');
        }
        {
            let w2 = work.next().unwrap(); //" CHN :MSG\r\n"
//            let mut w2a = w2.splitn(2, ":"); // TODO: use when splitn() bug fixed.
            // Thanks to steveklabnik for this work around
            let mut w2a = w2.splitn(2, {|x| x == ':'});
            chn = w2a.next().unwrap().trim_matches(' ');
            msg = w2a.next().unwrap().lines_any().next().unwrap();
        }

        let retval: ChanData = ChanData
        {
            msg: msg,
            hst: hst,
            chn: chn,
            usr: usr,
        };
        retval
    } else {
        // For now, just return the raw socket data...
        let retval: ChanData = ChanData
        {
            msg: data,
            hst: "NULLHST",
            chn: "NULLCHN",
            usr: "NULLUSR",
        };
        retval
    }
}

