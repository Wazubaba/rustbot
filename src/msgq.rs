use std::io::prelude::*;
use std::net::TcpStream;

const DEBUG: bool = true;

pub fn getl(mut socket: TcpStream) -> String
{ // Read from the socket untill finding a \n char.
    let mut retval = String::new();
    let mut buffer = [0; 1];
    loop {
        let rlen = socket.read(&mut buffer).unwrap();
        if DEBUG {
            if rlen > 1 {
                println!("\n\t[msgq:getl()]->WARN:potentially lost data, rlen == {}\n", rlen);
            }
        }
        let chr = buffer[0] as char;
        if chr == '\n'{
            retval.push('\n');
            break;
        } else {
            retval.push(chr);
        }
    }
    return retval
}

