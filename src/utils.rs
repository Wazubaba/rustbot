use std::io::prelude::*;
use std::net::TcpStream;
//use std::str;

/*
pub const MAXBUF: usize = 256;
*/

pub fn subs(start: u32, end: u32, s: &str) -> String
{// returns a substring `string` from a `&str`
    let mut i = 0;
    let mut retval = "".to_string();
    for c in s.chars() {
        if i >= start {
            if i > end { break }
            retval.push(c);
        }
        i += 1;
    }
    retval
}
/*
pub fn get(mut socket:TcpStream) -> String
{
    // Retrieves data from the socket, up to `MAXBUF` bytes
    let min:u8 = 0;
    let mut buffer = [min; MAXBUF];

    let _ = socket.read(&mut buffer);
    str::from_utf8(&buffer).unwrap().to_string()
}
*/
pub fn put(mut socket:TcpStream, msg:&str)
{
    // Sends data `msg` on the socket
    let buffer = msg.as_bytes();
    let _ = socket.write(&buffer);
    let _ = socket.flush();
}

